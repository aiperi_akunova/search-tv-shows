import './App.css';
import {useReducer} from "react";
import axios from "axios";
import {NavLink} from "react-router-dom";
import {Route} from "react-router";
import TvShowForm from "./components/TvShowForm/TvShowForm";


const FETCH_TV_REQUEST = 'FETCH_TV_REQUEST';
const FETCH_TV_SUCCESS = 'FETCH_TV_SUCCESS';
const FETCH_TV_FAILURE = 'FETCH_TV_FAILURE';

// const FETCH_ONE_SHOW_REQUEST = 'FETCH_ONE_SHOW_REQUEST';
// const FETCH_ONE_SHOW_SUCCESS = 'FETCH_ONE_SHOW_SUCCESS';
// const FETCH_ONE_SHOW_FAILURE = 'FETCH_ONE_SHOW_FAILURE';

const SET_OPTIONS = 'SET_OPTIONS';

const fetch_tv_request = () => ({type: FETCH_TV_REQUEST});
const fetch_tv_success = showsData => ({type: FETCH_TV_SUCCESS, payload: showsData});
const fetch_tv_failure = () => ({type: FETCH_TV_FAILURE});

// const fetch_one_show_request= ()=>({type: FETCH_ONE_SHOW_REQUEST});
// const fetch_one_show_success = oneShowData =>({type: FETCH_ONE_SHOW_SUCCESS, payload: oneShowData});
// const fetch_one_show_failure = ()=>({type: FETCH_ONE_SHOW_FAILURE});

const INPUT_CHANGE = 'INPUT_CHANGE';
const inputChange = (value) =>({type: INPUT_CHANGE, payload: value})

const set_options =(value)=>({type: SET_OPTIONS, payload: value})

const initialState = {
    shows: [],
    error: null,
    loading: false,
    inputValue: '',
    optionsOpen: false
}

const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_TV_REQUEST:
            return {...state, error: null, loading: true};
        case FETCH_TV_SUCCESS:
            return {...state, loading: false, shows: action.payload};
        case FETCH_TV_FAILURE:
            return {...state, loading: false, error: action.payload};
        case INPUT_CHANGE:
            return {...state, inputValue: action.payload};
        case SET_OPTIONS:
            return {...state, optionsOpen: action.payload}
        default:
            return state;
    }
}

const App = () => {
  const [state, dispatch] = useReducer(reducer,initialState);
    const fetchShows = async (value)=>{

            dispatch(fetch_tv_request());
            try{
                const response = await axios.get('http://api.tvmaze.com/search/shows?q='+value);
                console.log('response', response.data)
                if(response.data === null){
                    dispatch(fetch_tv_success([]))
                }else{
                    dispatch(fetch_tv_success(response.data));
                    console.log('in success',response.data)
                }
            }catch (e){
                dispatch(fetch_tv_failure())
            }
    }

    // const currentShowRequest = async (id)=>{
    //     try{
    //         const response = await axios.get('http://api.tvmaze.com/shows/'+id);
    //         console.log('response one show', response.data)
    //         if(response.data === null){
    //             dispatch(fetch_one_show_success([]))
    //         }else{
    //             dispatch(fetch_one_show_success(response.data));
    //             console.log('oneShow',response.data)
    //         }
    //     }catch (e){
    //         dispatch(fetch_one_show_failure())
    //     }
    // }



    let showsOptions = null;
    const onInputChange = (value)=>{
        dispatch(inputChange(value));
        if(state.inputValue.length>=3) {
            fetchShows(state.inputValue);
            dispatch(set_options(true))
        } else if(state.inputValue.length===0){
            showsOptions = null;
            dispatch(set_options(false))
        }
    }

    const onNavlinkClick = (value)=>{
        dispatch(set_options(false));
        dispatch(inputChange(value));

    }

    showsOptions = state.shows.map(obj=>(
        <div className='option' id={obj.show.id} key={obj.show.id}>
            <NavLink to={'/shows/'+obj.show.id} onClick={()=>onNavlinkClick(obj.show.name)}>
                {obj.show.name}
            </NavLink>
        </div>
    ));

    return (
        <div className="App">
            <h1 className='main-title'>TV-Shows</h1>
            <div className='search-box'>
                <div className='search'>
                    <label className='search-label'>
                        Search for TV-Shows:
                    </label>
                    <input
                        className='search-input'
                        type='text'
                        value = {state.inputValue}
                        onChange={(e)=>onInputChange(e.target.value)}
                    />
                </div>
                    {state.optionsOpen && (
                        <div className='shows-options'>{showsOptions}</div>)}
            </div>

            <div>
                    <Route path="/shows/:id" component={TvShowForm}/>
            </div>

        </div>
    );
};

export default App;
