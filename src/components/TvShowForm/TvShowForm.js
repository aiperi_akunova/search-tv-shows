import React, {useEffect, useState} from 'react';
import './TvShowForm.css';
import axios from "axios";
import Parser from 'html-react-parser';


const TvShowForm = ({match}) => {

    const [show, setShow] = useState(null);
    const text = show && '`'+show.summary+'`';

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get('http://api.tvmaze.com/shows/' + match.params.id);
            setShow(response.data);
        }
        fetchData();

    }, [match.params.id]);


    return show && (
        <div className='show-form-box'>
            <div className='box1'>
                <img src={show.image && show.image.medium} alt='shows'/>
            </div>
            <div className='box2'>
                <h3>{show.name}</h3>
                <div>{Parser(text)}</div>
            </div>
        </div>
    );
};

export default TvShowForm;